package com.hipolitoronquillohernandez.apprecycleview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val arrayList = ArrayList<Materia>()
        arrayList.add(Materia("Programación Web", "Jorge Mario Figueroa Garcia", R.drawable.web))
        arrayList.add(Materia("Sistemas programables", "Jorge Cruz Salazar", R.drawable.sistemas))
        arrayList.add(Materia("Conmutación y Enrutamiento en Redes", "Jose Juan Reyes Torres", R.drawable.redes))
        arrayList.add(Materia("Inteligencia Artificial", "Roberto Angel Melendez Armenta ", R.drawable.ia))
        arrayList.add(Materia("Fundamentos a la Ciencia de Datos", "Simon Pedro Arguijo Hernandez", R.drawable.ciencia))
        arrayList.add(Materia("Desarrollo de Aplicaciones Móviles", "Irahan Otoniel Jose Guzman", R.drawable.moviles))
        arrayList.add(Materia("Taller de Investigación", "Xochilt Garcia Guzman", R.drawable.taller))

        val myAdapter = MyAdapter(arrayList, this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = myAdapter
    }
}