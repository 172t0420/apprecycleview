package com.hipolitoronquillohernandez.apprecycleview

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.lista_item.view.*

class MyAdapter(val arrayList: ArrayList<Materia>, val context: Context):
    RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bindItems(materia: Materia){
            itemView.txtTitulo.text=materia.titulo
            itemView.txtSubtitulo.text=materia.subtitulo
            itemView.imgWeb.setImageResource(materia.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v=LayoutInflater.from(parent.context).inflate(R.layout.lista_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(arrayList[position])
        holder.itemView.setOnClickListener{
            if(position==0) {
                Toast.makeText(context, "Programación Web", Toast.LENGTH_SHORT).show()
                val intent= Intent(context, PWActivity::class.java)
                context.startActivity(intent)
            }
            else if(position==1) {
                Toast.makeText(context, "Sistemas programables", Toast.LENGTH_SHORT).show()
                val intent= Intent(context, SPActivity::class.java)
                context.startActivity(intent)
            }
            else if(position==2){
                Toast.makeText(context,"Conmutación y Enrutamiento en Redes", Toast.LENGTH_SHORT).show()
                val intent= Intent(context, CRActivity::class.java)
                context.startActivity(intent)
                }
            else if(position==3) {
                Toast.makeText(context, "Inteligencia Artificial", Toast.LENGTH_SHORT).show()
                val intent= Intent(context, IAActivity::class.java)
                context.startActivity(intent)
            }
            else if(position==4){
                Toast.makeText(context,"Fundamentos a la Ciencia de Datos", Toast.LENGTH_SHORT).show()
                val intent= Intent(context, CDActivity::class.java)
                context.startActivity(intent)
                }
            else if(position==5){
                Toast.makeText(context,"Desarrollo de Aplicaciones Móviles", Toast.LENGTH_SHORT).show()
                val intent= Intent(context, DAMActivity::class.java)
                context.startActivity(intent)
                }
            else if(position==6) {
                Toast.makeText(context, "Taller de Investigación", Toast.LENGTH_SHORT).show()
                val intent= Intent(context, TIActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }
}